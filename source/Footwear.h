/*
 * Footwear.h
 *
 *  Created on: 22 Oct 2018
 *      Author: Blas
 *      Description: Footwear Header Class child from Clothes Class with the inheritance of the States & Function
 *      			and the addtion of new States/Functions.
 *      			STATES
 *      			string material:
 *      			string customer:
 *
 *      			METHODS:
 *      			Footwear contructor
 *      			~Footwear destructor
 *      			string getPrice(): Abstract Method defined at the parent Clothes Class to return a string with the value & currency of the item
 */

#include<string>
using std::string;
#include "Clothes.h"

#ifndef FOOTWEAR_H_
#define FOOTWEAR_H_

namespace blaspace {


class Footwear: public Clothes {
protected :
	string customer;
private:
	string material;
public:
	Footwear(string,string,float,string,string);
	Footwear(string,string); // Overload constructor with default values
	Footwear();	// Overloaded constructor with default values
	virtual ~Footwear();
	virtual void display();
	virtual float getPrice();
	void updatePrice(float);
	virtual string getCustomer();

};

} /* namespace blaspace */

#endif /* FOOTWEAR_H_ */
