/*============================================================================
// Name        : Assigment_1.cpp
// Author      : Blas Molina
// Version     :
// Copyright   : Your copyright notice
// Description : To demostrate Class States,Methodes Object declaration, Inheritance, Encapsulation and Polymorphism.
//
// My code is based in Clothes objects.
 * The main/principal/generic class is Clothes which has the general States:
//	- size : Depending on the subtype of class this value could be a string/char:{S,M,L,XL}
//	- color : The color can only be defined as String:{Black,White,Blue,Red,Yellow,Gold,Silver...}
//	- price : This is a Float type with the price of the item or total when more than 1 item
// 	- N_items :Number of items. It ould be used in the shops for calculations of total number of clothes or stock
 * The Methodes associated to this class are:
 * - DisplayCloth : Present main Information from the Class Clothes: size,color,Brand,price,designer,body_part
 * - AddItems : Adding 2 types of clothes from same or different subtype. This can be defined as abstract
 *
// Subtype Class : Clothes->Footwear
 * This subtype class has the particular States:
 * - type : String defining the type of shoes:{Shoes, boots, sandals, ...}
 * - material/fabric : String type defining what is made of the shoes:{leather, plastic,wood...}
 * - soles : {rubber, plastic...}
 * - heel : {length of the heel in cm: 2,4,6,... }
 * - laces : Indicate if it uses laces, boolean
 * write some Methodes!!
 *
// Subtype Class : Clothes->Footwear->Boots
 * This subtype has the particular States:
 * - fastening_method : String type indicating if the Boots has : {Zip, laces,bucklets}
 * write some method!!
 *
// Subtype Class : CLothes->Footwear->Shoes
  * This subtype has the particular States:
 * - numEyelets : Integer indicating number of eyelets
 * write some method!!
 *
// Subtype Class : Clothes->Trousers
 * This subtype class has the particular States:
 * - design_type : String defining the type of trousers:{jeans,chinos ...}
 * - material/fabric : String type defining what is made of :{cotton, elastic, ...}
 * - n_pockets : Integer type indicating the number of pockets:{2,3,4,5..}
 *
 * write some Methodes!!
 *
 // Subtype Class : CLothes->Trousers->Jeans
  * This subtype has the particular States:
 * - cuts_config : int type indicating how many cuts : {0,1,2,3..}
 * write some method!!
 *
 // Subtype Class : CLothes->Trousers->Chinos
  * This subtype has the particular States:
 * -
 * write some method!!
 *
 *
// Diagram:
 * 										Clothes --------> Designer Is PART OF Class Clothes
 *								-----------|----------
 *								|					  |
 *							Trousers			Footwear
 *						--------|	  		 -------|------
 *						|			          |			|
 *					Jeans			  		Boots		 Shoes
 *
// Demonstration of C++ aspects:
 *
 * 1) Correct Use of Access specifiers: Public,private,protected
 *	The class Clothes has Private its states to keep them hidden/protected from the users of the class
 *	There are also some Protected States for the subclasses: FootWear & Trousers to use them in a function getPrice()
 *	The Methods from class Clothes are public to allow the users interact with the Class, also to be used in the subClasses Methods
 *
 * 2) Over-loading using either Methods or Constructor
 * The class Clothes has 2 methods called "add" which overload their function. If two strings are provided,
 * the method return a single string. If two floats are provided, the method returns the addition of these values
 * Clothes Trousers and Footwear constructors are also over-loaded with definitions where parameters have a default value.
 *
 * 3) Abstract Class with a method required in all derived classes
 * 	The class Clothes is an abstract class because the Method getPrice()=0; is not Implemented and it is indicated to the compiler a such
 * 	This method is Implemented in both Children class: FootWear and Trousers to instantiate objects of these classes. Even more, the
 * 	protected parameters are used on these methods to return a string type.
 *
 * 4) Over-riding a Method
 * 	Method display() is over-ride at the children classes Footwear and Shoes adding some more information to the Parent Method
 * 	This is done using the same function name display() and also defining the parent method and the new features, like printing
 * 	new information
 *
 * 5) Example Multiple Inheritance
 * No implemented
 *
 *
 * 6) Separate compilation all classes
 * 	Separete compilation for all classes is provided through each Class being defined with a *.h(header) and the Methods declaration
 * 	for each header *.cpp . The Classes with their parents are included through the command:
 * 	#include "Trousers.h" and #include "Shoes.h" which call at the same time their parents
 *
 * 7) Example of friend function that receives an object and modifies the object
 * Class "Shoes" has a friend function called changeLets which allows the modification of the private state EyeLets number
 * from the main section.
 *
 * 8) Class with a modified COPY constructor demonstrating the effects of pass-by-value and pass-by-reference
 * The Copy Construct at the Class "Trousers" has been modified to add +2 pockets to all new copies.
 * Functions print(Trousers a):Pass-by-Value and printRef(Trousers &a):Pass-by-Reference take these objects class and display its information, including the memory address
 * With the Pass-by-Value function, the Copy construct is called which modifies the number of pockets and creates an object
 * in a different memory location.
 * With the Pass-by-Reference, there is no call to the Copy Constructor, so that the functio works with the original object. Memory location does not change
 *
 * 9) Working destructor with basic functionality on one class. Demonstrate it in the main()
 * 	A working destructor is implemented at the Trousers Class using the notation: Trousers::~Trousers.
 * 	The Destructor prints a message about the item that has been returned to the shop : [A pair of Trousers was destroyed from <customer>]
 *
 * 10) Correct use of three over-loaded operators (==,+,<)for one of the classes
 * Class "Shoes" has defined the 3 operators above to compare if 2 ocustomers are the same : "=="
 * If a customer has bought 2 or more pairs of Shoes : "+"
 * or which of two customers should be first in a queue : "<"
 *
 *
 * 11) Operation on pointers to array of objects
 * Creating an Array of five Trousers Objects and printing the information from each one using a fix pointer to the first element
 * while iterating through the array.
 *
 *
 * 12) Use four C++ explicit style casts: static, dynamic , constant and reinterpret
 * static_cast : narrowing conversion in the updatePrice() method from the Footwear class. Rounding the price
 * from float to int with int(x) casting method.
 * dynamic_cast : Donw-cast from Parent to Child Classes with Pointer values. Using the "Footwear" parent Class to
 * down-cast into Child Class "Shoes".
 * code ---> Footwear *cc = new Footwear(); 	Shoes *copy_cc = dynamic_cast<Shoes*>(cc);
 * const_cast:to cast away constant which are used within functions. In this case the  vatAddion function in the main program
 * adds the constant VAT amount to the class "Shoes".
 * code ---> vatAddition(*const_cast<int*>(&vat),bb);
 * reinterpret_cast : Converts any object into any other object. Converting a Class "Footwear" pointer into a double, not very usefull
 * code ---> double *f_ccc = reinterpret_cast<double*>(ccc);
 *
 *
 * 13) Use of Dynamic binding with virtual and non-virtual methods. Demonstrate
 * 	Dynamic binding is generated by the assignation of a Pointer Object type Footwear to the dynamic created object Shoes at line XX
 * 	Then this object is used with 2 methods: display() which it is a virtual method and updatePrice(), a non-virtual.
 * 	display() method is called from the Shoes object. Virtual word tells to call the closest method associated with the dynamic type
 * 	in this case Shoes Object. In the other hand, when non-virtual updatePrice() method is called, this is done from the static type, Footwear
 *
 * 	Code Dynamic biding---> Footwear* fp = new Shoes("Red","S",30.67,"Eva","leather",6);
 * 	Results:
 * 	The clothes are Red with a size S costing 30.670000		<--- Virtual call from the Child class "Shoes" with the Class pointer *fp
 * 	Customer is : Eva and the Footwear is made of leather
 * 	with all these 6 eyelets for the shoes.Wooh!!
 * 	Price updated from Footwear Class. New rounded price is 20 <----- Non-virtual function called from the Base class "Shoes"
 * 	from the pointer *fp
 *
 *
 * 14) Use of NEW and DELETE for object allocation with operations using pointers
 * This is done at the main() function the lines XX where a Shoes Object is created as a pointer Shoes p*
 * 	using the NEW assignation. Right after, the attributes for price and number of eyelets are updated.
 * 	A message is printed for confirmation. The Shoes pointer p* is deleted before ending the program
 *
 * 15) Static state of Class with an example usage. Demostrate the impact
 * State num_clothes is added to the Base class "Clothes" so that the total number of purchased/generated clothes can be counted
 * this is done with the static int num_clothes variable defined at the protected section,keeping the variable internal to the Class
 * For keeping the count of any new clothes purchased, an internal function is declared at the private section fot the same Class "Clothes" : updateClothes();
 * This method is added to the Constructor to have the count of any item from child classes : Shoes, Trousers and Footwear.
 * Results: a message is printed with the Total number of clothes purchased with the display() method.
 * The clothes are White with a size M costing 50.78
 * Total number of Clothes purchased are = 11		<---- Total numer of Clothes
 * Customer is : Clara and the Footwear is made of plastic
 * with all these 20 eyelets for the shoes.Wooh!!
 * The clothes are Red with a size S costing 30.67	<--- New item bough
 * Total number of Clothes purchased are = 12		<--- Message generated with the new total number of items
 *
 *
 * 16) Demonstrate differences between C++ Class and C++ struct
 * New Struct "Jackets" is added to the group without specifying any members types, these are all public by default. So that they can be accessed from
 * the main().The struct has the states : customer, color and size which are modified in the main() function.
 * code C++ Struck --->
 * Jackets ff("Keith","Blue","XL");
 * ff.display();
 * ff.customer = "Jan";
 * ff.color = "Pink";
 * ff.size = "S";
 * ff.display();
 *
 * Results --->
 * [ New Jacket was created by Keith ]
 * A new Blue Jacket from Keith of size XL has been bought
 * A new Pink Jacket from Jan of size S has been bought
 *
 * 17) Passing an object to a method by CONSTANT reference. Demonstrate impact
 * The friend method newPocketDesign(const Trousers &a, int b) has a constant parameter with the reference value of the object. So that
 * the original trousers are not modified. The program prints the same number of pockets, before and after the method. If the method tries to
 * modify any state from the Trousers reference Object, the compilation processes issues an error, see bellow for more details.
 * code for CONSTANT reference method --->
 * Trousers *g = new Trousers("Black","M",78.98,"Tom",2);
 * g->display();
 * newPocketsDesign(*g,4);		// friendly function with a Constant Trousers Object
 * g->display();
 *
 * Results ---> without modification of the Object
 * The clothes are Black with a size M costing 78.98
 * Total number of Clothes purchased are = 13 and the trousers have 2 pockets
 * Tom trousers have the address 0x316b0
 * Old Trousers designs has 2 but customer wants to add 4 pockets
 * New Trousers design should have 6
 * The clothes are Black with a size M costing 78.98
 * Total number of Clothes purchased are = 13 and the trousers have 2 pockets
 * Tom trousers have the address 0x316b0
 *
 * Results ---> when the reference object tries to be modified: it generates a compilation error
 * ..\src\Trousers.cpp:62:59: error: assignment of member 'blaspace::Trousers::pockets' in read-only object
 * cout <<"New Trousers design should have "<<(a.pockets += b)<<endl;
 *
 * 18) Write straightforward example class template
 * Class Template with name Drinks has been defined in the Header file "Drinks.h". The Template has also been added to the main program
 * It has Private States: string type and float vol
 * Also Public methods : Get and Set as well as the Constructor and Destructors
 *
 * 19) Use VECTOR CONTAINER to contain a number of objects of one of your class
 * A vector container of Trousers is created and 7 different Trousers are added. The Trousers are displayed then
 * code --->
 * vector<Trousers> vect_Tr;
 * Trousers x1("Brown","M",23.56,"Mike",2);
 * vect_Tr.push_back(Trousers(x1));
 * Trousers x2("Black","XL",79.8,"Tom",4);
 * vect_Tr.push_back(Trousers(x2));
 * ...........
 *
 * 20) Use SORT algorithm on your container
 * sort(vect_Tr.begin(),vect_Tr.end());		// Trousers object are sorted by name using the over-loaded operator <
 * for_each(vect_Tr.begin(), vect_Tr.end(), outputFunct);
//============================================================================ */


// General packages
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

// Blas Objects Headers
#include "Shoes.h"
#include "Trousers.h"
#include "Jackets.h"		// C++ Struct  <---- point 16
#include "Drinks.h"			// Class Template <---- point 18
using namespace blaspace;


void print(Trousers a){
	a.display();
}
void printRef(Trousers &a){
	a.display();
}

void vatAddition(int &a, Shoes &b){
	float currentPrice= b.getPrice();
	cout<<"Old price is "<<currentPrice<<"and the VAT is "<<a<<endl;
	float newPrice = currentPrice + (currentPrice * (static_cast<float>(a)/100)); // static_cast from int to float
	cout<<"New price with VAT is "<<newPrice<<endl;
	b.updatePrice(newPrice);
}
void outputFunct(Trousers a){
	a.display();
}

int main() {
	const float minPrice = 30.55;
	const int vat = 18;

	Shoes a("Black","S",45.78,"Ela","plastic",6);	// Instantiation of Shoes Object
	a.display();
	changeLets(a,12);	// Friend function modifying the eyelets from Ela Shoes
	a.display();

	/* Point 11 --- Testing Operations with pointers to array of Trousers objects 	*/
	Trousers someTrousers[5] = {			// Array of Trousers objects
			Trousers("Brown","L",78.56,"Mike",2),
			Trousers("Yellow","XS",34.9,"Lua",3),
			Trousers("Green","M",10.5,"Ben",1),
			Trousers("Pink","S",55.55,"Zoe",7),
			Trousers("Purple","L",78.5,"Noel",6)
	};
	Trousers *ptr1 = &someTrousers[0];
	for(int i=0; i<5;i++){
		(ptr1+i)->display();
	}
	/* End of Test Operations with Pointers of Array Trousers Objects	*/

	/* 	point 12 --- Testing explicit C++ style casting 	*/
	Shoes bb("Pink","L",67.89,"Nora","plastic",2);
	Footwear *cc= new Footwear(); //("Green","XL",23.56,"Erik","fabric");
	Shoes *copy_cc = dynamic_cast<Shoes*>(cc);		// Down casting from Parent to Child class using Dynamic Casting
	cout<<"Memory location for copy_cc is "<<&copy_cc<<" and for cc is "<<&cc<<endl;
	//copy_cc->getPrice();	// Program halts at when using down-casted Class
	delete copy_cc;		// *copy_cc Pointer destruction
	delete cc;			// *cc Pointer destruction
	vatAddition(*const_cast<int*>(&vat),bb);		// constant casting through a Function to add VAT to prices on Shoes
	Footwear *ccc = new Footwear();
	ccc->updatePrice(56.89);
	double *f_ccc = reinterpret_cast<double*>(ccc);	// reinterpret casting from Footwear Class pointer to a double pointer
	cout<<"Printing the Reinterpret cast value of a Footwear Pointer: "<<f_ccc<<endl;
	delete f_ccc;		// *f_ccc pointer destruction
	/* End of Test explicit C++ style casting 	*/

	/*	point 10 --- Testing over-loaded operators	*/
	Shoes aa("Brown","M",67.8,"Ela","fabric",4);
	if(a == aa){
		cout<<"They are the same owner: "<<a.getCustomer()<<endl;
	}
	Shoes aaa("Brown","M",67.8,"Peter","fabric",4);
	if(a<aaa){
		cout<<"Customer "<<a.getCustomer()<<"is before "<<aaa.getCustomer()<<endl;
	} else {
		cout<<"Customer "<<aaa.getCustomer()<<"is before "<<a.getCustomer()<<endl;
	}
	/*	 End  of test for over-loaded operators	*/

	Footwear b("Brown","L",minPrice,"Tom","plastic");	// Instantiation of Object class "Footwear"
	b.display();

	Footwear d("Luis","rubber");	// point 2 --- Instantiation of Object Footwear with over-loaded constructor
	d.display();

	Trousers c("Yellow","L",34.56,"Joe",8);
	c.display();
	/* point 8 ---- Copy constructor Test 	*/
	print(c); 	//Pass-by-value of the Object type Trousers, point 8: Copy constructor
	printRef(c); //Pass-by-reference of the Object type Trousers, point 8: Copy constructor
	/* 	End of Test for Copy constructor 	*/

	/* point 14 ---- Dynamic pointer with NEW and DELETE Test	*/
	Shoes* sp = new Shoes("White","M",45.23,"Clara","plastic",16);	//Shoes Object Pointer*
	sp->display(); // old price, pointer operation
	sp->updatePrice(50.78); //Update price, pointer operation with non-virtual method
	sp->addEyelets(4);		// add more eyelets, pointer operation
	sp->display();
	delete sp;				//Shoes Object pointer is destroyed, p*
	/* 	End of Dynamic pointer Test 	*/

	/* 	point 13 --- Dynamic Binding with virtual and non-virtual methods Test 	*/
	Footwear* fp = new Shoes("Red","S",30.67,"Eva","leather",6);		// Dynamic binding from Object Shoes to Object Footwear
	fp->display();			//Virtual method with dynamic binding object
	fp->updatePrice(20.45); 	// non-virtual method with dynamic binding object
	delete fp;
	/* 	End of Dynamic Binding with virtual and non-virtual methods Test 	*/

	/* point 16 ---  Differences between C++ Class and C++ Struct Test 	*/
	Jackets ff("Keith","Blue","XL");	// Jacket struct object where states are Public by default
	ff.display();
	ff.customer = "Jan";				// ff Object states modification from main()
	ff.color = "Pink";
	ff.size = "S";
	ff.display();
	/* End of C++ Class and C++ Struct Test	*/

	/* point 17 --- Passing an object to a method by CONSTANT reference 	*/
	Trousers *g = new Trousers("Black","M",78.98,"Tom",2);
	g->display();
	newPocketsDesign(*g,4);		// friendly function with a Constant Trousers Object
	g->display();
	delete g;
	/* End of Test for Method with CONSTANT reference	*/

	/* point 19 --- Vector container with some Class Objects 	*/
	vector<Trousers> vect_Tr;	// Declaration vector container of Object type Trousers
	Trousers x1("Brown","M",23.56,"Mike",2);
	vect_Tr.push_back(Trousers(x1));
	Trousers x2("Black","XL",79.8,"Tom",4);
	vect_Tr.push_back(Trousers(x2));
	Trousers x3("Blue","S",45.7,"Noel",1);
	vect_Tr.push_back(Trousers(x3));
	Trousers x4("Yellow","XS",56.9,"Aiona",5);
	vect_Tr.push_back(Trousers(x4));
	Trousers x5("Pink","M",32.5,"Zoe",7);
	vect_Tr.push_back(Trousers(x5));
	Trousers x6("Red","L",69.7,"Niall",8);
	vect_Tr.push_back(Trousers(x6));
	Trousers x7("Purple","S",56.2,"Eva",3);
	vect_Tr.push_back(Trousers(x7));

	 for_each(vect_Tr.begin(), vect_Tr.end(), outputFunct);
	 /* End of Testing Vector container with Trousers 	*/

	 /* point 20 --- SORT algortihm to order elements of vector 	*/
	 sort(vect_Tr.begin(),vect_Tr.end());	// Trousers object are sorted by name using the over-loaded operator <
	 for_each(vect_Tr.begin(), vect_Tr.end(), outputFunct);
	 /* End of Testing SORT algorithm 	*/

/*	cout<<"write 5 Trousers with format: ['color','size',price,'customer',pockets]"<<endl;
	for (int i = 0; i<5; i++){
		//cin>>x;
		vect_Tr.push_back(Trousers(x));
	}
*/
	cout<<"END of program"<<endl;
	return 0;
}



