/*
 * Trousers.h
 *
 *  Created on: 22 Oct 2018
 *      Author: Blas
 *      Description: Trousers Header Class, child from Clothes Class with the inheritance of the States & Function
 *      			and the addtion of new States/Functions.
 *      			STATES
 *      			string customer:
 *      			int pockets
 *
 *      			METHODS:
 *      			Trousers contructor
 *      			~Trousers destructor
 *      			void display(): Display the data from the Class Clothes and Trousers.
 *      							over-riding display() method from Clothes Class with additional info
 *      			string getPrice(): Abstract Method defined at the parent Clothes Class to return a string with the value & currency of the item
 */

#include "Clothes.h"
#include <iostream>
using std::string;

#ifndef TROUSERS_H_
#define TROUSERS_H_

namespace blaspace {

class Trousers: public Clothes {
private:
	string customer;
	int pockets;
public:
	Trousers(string,string,float,string,int);
	Trousers(string,int);
	Trousers();
	Trousers(const Trousers &sourceTrousers); // Modified copy constructor
	virtual ~Trousers();

	// Over-loaded operators
	virtual bool operator == (Trousers);
	virtual bool operator < (Trousers);

	virtual void display();			// over-riding display() method from Clothes Class with additional info
	virtual float getPrice();		// Abstract Method defined at the parent Clothes Class to return a string
	friend void newPocketsDesign(const Trousers&,int);	//Constant reference method with friend function
};

} /* namespace blaspace */

#endif /* TROUSERS_H_ */
