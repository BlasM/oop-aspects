/*
 * Designer.cpp
 *
 *  Created on: 22 Oct 2018
 *      Author: Blas
 *      Description: Methods and Functions Implementation for the Designer Class
 *      These are:
 *      Designer::Designer = Constructor Class
 *      Designer::~Designer = Destructor method
 *      Designer::display() = Print the Name and ref Number of the designer
 */

#include "Designer.h"
#include <iostream>
using namespace std;

namespace blaspace {


Designer::Designer(string name, int refNum):
		name(name),refNum(refNum){// TODO Auto-generated constructor stub

}
Designer::~Designer() {
	// TODO Auto-generated destructor stub
}
void Designer::display(){
	cout<<"Designer name is: "<<name<<" with the ref number: "<<refNum<<endl;
}
/*void Designer:changeClothes(Clothes &cloth, string newcolor){
		cloth.color =  newcolor;
} */

} /* namespace blaspace */
