/*
 * Trousers.cpp
 *
 *  Created on: 22 Oct 2018
 *      Author: Blas
 *      Description: Methods and Functions Implementation for the Trousers Class
 *      These are:
 *      Trousers::Trousers = Constructor Class with Member Initialization List
 *      Trousers::~Trousers = Destructor method for the virtual Methods
 *      Trousers::display() = Print the Color, size and price of the Clothes class with
 *      						the addition of the material. Over-Riding Clothes method
 *      Footwear::getPrice() = It use the protected Attributes Price & Currency to generate the price of the item.
 *      						This method is defined as abstract at CLoths class so that need to be implemented here
 *      						to be able to instantiated objects of type Footwear
 */
#include<iostream>
using namespace std;
#include "Trousers.h"

namespace blaspace {

Trousers::Trousers(string color,string size,float price,string customer,int pockets):
			Clothes(color,size,price),customer(customer),pockets(pockets)
{			cout<<"[ New pair of Trousers was created by "<<customer<<" ]"<<endl; }
Trousers::Trousers(string customer,int pockets):
			Clothes(),customer(customer),pockets(pockets)
{			cout<<"[ New pair of Trousers was created by "<<customer<<" ]"<<endl; }
Trousers::Trousers():
			Clothes(),customer("Not defined"),pockets(0){}

Trousers::Trousers(const Trousers &sourceTrousers):	// Copy constructor with the modification of pockets +2 for each copy
	Clothes(sourceTrousers.color,sourceTrousers.size,sourceTrousers.price)
{
	this->customer = sourceTrousers.customer;// (sourceTrousers.customer),
	this->pockets = sourceTrousers.pockets + 2; //pockets(sourceTrousers.pockets + 2){
	cout<<"[ COPY Constructor for New pair of Trousers was created by "<<customer<<" ]"<<endl;
}

Trousers::~Trousers() {
	cout<<"[ A pair of Trousers was destroyed from "<<customer<<" ]"<<endl;	//Working destructor printing a message
}

// Implementation of over-loaded operators : "==" ; "+" ; "<"
bool Trousers::operator == (Trousers a){
	return(this->customer == a.customer);
}
bool Trousers::operator <(Trousers a){
	return (this->customer < a.customer);
}

void Trousers::display(){
	Clothes::display();
	cout<<" and the trousers have "<<pockets<<" pockets"<<endl;
	cout<<customer<<" trousers have the address "<<this<<endl;
}
float Trousers::getPrice(){
	return(price);
}
// Friend Method to suggest a modification in the number of pockets without modif the original design
void newPocketsDesign(const Trousers &a, int b){
	cout<<"Old Trousers designs has "<<a.pockets<<" but customer wants to add "<<b<<" pockets"<<endl;
	cout <<"New Trousers design should have "<<(a.pockets + b)<<endl;
}

} /* namespace blaspace */

