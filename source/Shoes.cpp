/*
 * Shoes.cpp
 *
 *  Created on: 22 Oct 2018
 *      Author: Blas
 *      Description: Methods and Functions Implementation for the Shoes Class
 *      These are:
 *      Shoes::Shoes = Constructor Class with Member Initialization List
 *      Shoes::~Shoes = Destructor method for the virtual Methods
 *      Shoes::display() = Print the Color, size and price of the Clothes class, material and heel from Footwear Class
 *      					 and adds the eyelets property . Over-Riding Clothes/Footwear method
 */


#include "Shoes.h"
#include <iostream>
using namespace std;

namespace blaspace {

Shoes::Shoes(string color,string size,float price,string customer,string material,int eyelets):
		Footwear(color,size,price,customer,material),eyelets(eyelets),items(1) {}
Shoes::~Shoes() {
	// TODO Auto-generated destructor stub
}

// Implementation of over-loaded operators : "==" ; "+" ; "<"
bool Shoes::operator == (Shoes a){
	return(this->customer == a.customer);
}
int Shoes::operator + (Shoes a){
	return (this->customer == a.customer?(items=+a.items):0);
}
bool Shoes::operator <(Shoes a){
	return (this->customer < a.customer);
}

void Shoes::display(){
	Footwear::display();
	cout<<"with all these "<<eyelets<<" eyelets for the shoes.Wooh!!"<<endl;
}
void Shoes::updatePrice(float nprice){
	price = nprice;
	cout<<"Price updated from Shoes Class. New price is "<<price<<endl;
}
void Shoes::addEyelets(int a){
	cout<<"The Shoes had "<<eyelets<<endl;
	eyelets = eyelets + a;
	cout<<"New number of Eyelets is "<<eyelets<<endl;
}

// friend function that changes the number of eyelets of class Shoes//
void changeLets(Shoes &a, int changelets){
	a.eyelets = changelets;
}


} /* namespace blaspace */
