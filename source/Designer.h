/*
 * Designer.h
 *
 *  Created on: 22 Oct 2018
 *      Author: Blas Molina
 *      Decription: Designer Header Class with the States/Properties definition
 *      			these are: string name; int refNum
 */

#include<string>
using std::string;
//#include "Clothes.h";


#ifndef DESIGNER_H_
#define DESIGNER_H_

namespace blaspace {



class Designer{
private:
	string name;
	int refNum;
public:
	Designer(string,int);
	virtual ~Designer();
	virtual void display();
	//virtual void changeClothes(Clothes &);
};

} /* namespace blaspace */

#endif /* DESIGNER_H_ */
