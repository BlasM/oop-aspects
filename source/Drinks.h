/*
 * Drinks.h
 *
 *  Created on: 4 Nov 2018
 *      Author: Blas
 */
#include<iostream>
using std::string;
using std::cout;
using std::endl;

#ifndef DRINKS_H_
#define DRINKS_H_

template <class T>
class Drinks{
private:
	string type;
	float vol;
public:
	Drinks(string,float);
	virtual ~Drinks();
	virtual string getType();
	virtual void setVol(float);
};

template <class T>
Drinks<T>::Drinks(string type,float vol):
	type(type),vol(vol){}
template <class T>
Drinks<T>::~Drinks(){
	cout<<"[General Destructor for Drinks Template is being called] "<<endl;
}
template <class T>
string Drinks<T>::getType(){
	cout<<"Drink type: "<<type<<endl;
	return(type);
}
template <class T>
void Drinks<T>::setVol(float a){
	vol = a;
}
#endif /* DRINKS_H_ */
