/*
 * Jackets.cpp
 *
 *  Created on: 4 Nov 2018
 *      Author: Blas
 */

#include "Jackets.h"
#include <iostream>
using namespace std;

namespace blaspace{

Jackets::Jackets(string customer,string color,string size):
				customer(customer),color(color),size(size){
	cout<<"[ New Jacket was created by "<<customer<<" ]"<<endl;
}

Jackets::~Jackets(){
	cout<<"[ A Jacket was destroyed from "<<customer<<" ]"<<endl;}

void Jackets::display(){
	cout<<" A new "<< color<< " Jacket from "<<customer<<" of size "<<size<< " has been bought"<<endl;
}

} /* end of blaspace */

