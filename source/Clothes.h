/*
 * Clothes.h
 *
 *  Created on: 22 Oct 2018
 *      Author: Blas
 *      Description: Clothes Header Class with the States/Properties definition
 *      			these are:
 *      			string color:
 *      			string size:
 *      			float price:
 *      			string currency:
 *      			//string brand
 *      			//int n_items;
 *      			//Designer dname;  This is an integrated Class which IS PART OF the Clothes class.
 *      			 As Such the designer Class may have the States: string name; string addres; int ref;
 */
#include<string>
using std::string;
#include "Designer.h"

#ifndef CLOTHES_H_
#define CLOTHES_H_

namespace blaspace {

class Clothes {
	// States are made private/protected from users. Methods are public to allow interaction with the states(interface)//
private:
	void updateClothes();		//Internal private method to keep the count of Clothes bought: Shoes, Footwear or Toursers
protected:
	static int num_clothes;		//Static state to keep the count of all clothes bought/purchased
	string color;
	string size;
	float price;
public:
	Clothes(string,string,float);
	Clothes(); // Over-load constructor with default values

	virtual ~Clothes();
	virtual void display();
	virtual string add(string a,string b);
	virtual float add(float a,float b);
	virtual float getPrice()=0;// Abstract method declaration so that makes the class also abstract
								// it forces the children to implement it
	virtual string getColor();
	virtual string getSize();
};

} /* namespace blaspace */

#endif /* CLOTHES_H_ */

