/*
 * Footwear.cpp
 *
 *  Created on: 22 Oct 2018
 *      Author: Blas
 *      Description: Methods and Functions Implementation for the Footwear Class
 *      These are:
 *      Footwear::Footwear = Constructor Class with Member Initialization List
 *      Footwear::~Footwear = Destructor method for the virtual Methods
 *      Footwear::display() = Print the Color, size and price of the Clothes class with
 *      						the addition of the material. Over-Riding Clothes method
 *      Footwear::getPrice() = It use the protected Attributes Price & Currency to generate the price of the item.
 *      						This method is defined as abstract at CLoths class so that need to be implemented here
 *      						to be able to instantiated objects of type Footwear
 */

#include "Footwear.h"
#include<iostream>
using namespace std;

namespace blaspace {


Footwear::Footwear(string color,string size,float price,string customer,string material):
			Clothes(color,size,price),customer(customer),material(material){}

Footwear::Footwear(string customer,string material):
			Clothes(),customer(customer),material(material){}
Footwear::Footwear():
	Clothes(),material("Not defined"){}
Footwear::~Footwear() {
	// TODO Auto-generated destructor stub
}

void Footwear::display(){
	Clothes::display();
	cout<<"Customer is : "<<customer<<" and the Footwear is made of "<<material<<endl;
}

float Footwear::getPrice(){
	return (price);
}

void Footwear::updatePrice(float nprice){
	price = static_cast<int>(nprice);	// static casting from float to int
	cout<<"Price updated from Footwear Class. New rounded price is "<<price<<endl;

}

string Footwear::getCustomer(){
	return(customer);
}

} /* namespace blaspace */
