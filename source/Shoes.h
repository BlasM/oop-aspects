/*
 * Shoes.h
 *
 *  Created on: 22 Oct 2018
 *      Author: Blas
 *      Description: Shoes Header Class child from Footwear Class with the inheritance of the States & Function
 *      			from Footwear and Clothes: Multiple inheritance
 *      			The addtion of new States/Functions.
 *      			STATES
 *      			int eyelets:
 *
 *      			METHODS:
 *      			Shoes contructor
 *      			~Shoes destructor
 *      			void display(): Display data from the Class Shoes
 */

#include<string>
using std::string;
#include "Footwear.h"

#ifndef SHOES_H_
#define SHOES_H_



namespace blaspace {


class Shoes: public Footwear {
private:
	int eyelets;
	int items;
public:
	Shoes(string,string,float,string,string,int);
	virtual ~Shoes();

	// Over-loaded operators
	virtual bool operator == (Shoes);
	virtual int operator + (Shoes);
	virtual bool operator < (Shoes);

	virtual void display();			// over-riding display() method from Clothes and Footwear Class with additional info
	void updatePrice(float);
	virtual void addEyelets(int);
	friend void changeLets(Shoes &, int);
};

} /* namespace blaspace */

#endif /* SHOES_H_ */

