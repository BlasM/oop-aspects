/*
 * Clothes.cpp
 *
 *  Created on: 22 Oct 2018
 *      Author: Blas
 *      Description: Methods and Functions Implementation for the Clothes Class
 *      These are:
 *      Clothes::Clothes = Constructor Class with Member Initialization List
 *      Clothes::~Clothes = Destructor method for the virtual Methods
 *      Clothes::display() = Print the Color, size and price of the Clothes
 */

#include "Clothes.h"
#include <iostream>
#include <string>
using namespace std;

namespace blaspace {

int Clothes::num_clothes = 0;

Clothes::Clothes(string color,string size,float price):
		color(color),size(size),price(price){ updateClothes();}

Clothes::Clothes():
		color("Not defined"),size("Not defined"),price(0.0){ updateClothes();}

Clothes::~Clothes() {
	// TODO Auto-generated destructor stub
}

void Clothes::updateClothes(){
	num_clothes += 1;
}

string Clothes::add(string a,string b){
	return(a+" & "+b);
}
float Clothes::add(float a,float b){
	return(a+b);
}

void Clothes::display(){
	cout<<"The clothes are "<<color<<" with a size "<<size<<" costing "<<getPrice()<<endl;
	cout<<"Total number of Clothes purchased are = "<<num_clothes<<endl;
}

string Clothes::getColor(){
	return(color);
}

string Clothes::getSize(){
	return(size);
}


} /* namespace blaspace */
