/*
 * Jackets.h
 *
 *  Created on: 4 Nov 2018
 *      Author: Blas
 */

#include<string>
using std::string;



#ifndef JACKETS_H_
#define JACKETS_H_

namespace blaspace{

struct Jackets{
	string customer;
	string color;
	string size;


	Jackets(string,string,string);
	virtual ~Jackets();
	virtual void display();


};
}/* end space blaspace */

#endif /* JACKETS_H_ */
